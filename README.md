# glossary

A glossary of abbreviations and other terms of art.


API - Application Programming Interface

ASR - Automatic Speech Recognition

DOW - Description of Work (also Description of Action)

ELG - European Language Grid. This Project! https://www.european-language-grid.eu/

IE - Information Extraction

IPR - Intellectual Property Rights

JSON - JavaScript Object Notation; preferred for structured interchange. See http://json.org/

HTML - HyperText Markup Language; ubiquitous

HTTP - HyperText Transfer Protocol; ubiquitous

I18N - Internationalisation

L10N - Localisation

LT - Language Technology

MIME - Multipurpose Internet Mail Extensions; but in this context likely to refer to MIME types in transfers via HTTP

MQ - Message Queue (see https://en.wikipedia.org/wiki/Advanced_Message_Queuing_Protocol)

MT - Machine Translation

MVC - Model, View, Controller (in the [Smalltalk sense](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller))

NCC - National Competence Centre (an ELG term)

NER - Named Entity Recognition

PDF - Portable Document Format; Adobe, ubiquitous

TSC - Tools, Services, Components (an ELG term)


